
const jogador1 = "X";
const jogador2 = "O";
var vez = jogador1;
var fimDeJogo = false;
var numeroDeJogadas = 0;
var reset = false;

indicarVez();
selecionar();

//Indica de quem é a vez de jogar
function indicarVez(){

	if(fimDeJogo){
		return;
	}
	//Verifica de quem é a vez
	if(vez == jogador1){

		//Mostra a imagem correspondente ao jogador X
		document.getElementById("indicadorDaVez").innerHTML="<img src='imagens/X.png'>"
		
	}
	else{

		//Mostra a imagem correspondente ao jogador O
		document.getElementById("indicadorDaVez").innerHTML="<img src='imagens/O.png'>"

	}

}

// Realiza uma jogada
function selecionar(botao) {
	
	//Cria array com as casas (posições)
	var casas = document.getElementsByClassName("casa");

	if(fimDeJogo){return;}

	//Testa se o botão já não foi marcado
	if(botao.getElementsByTagName("img").length == 0){
		
		//Condicional para marcar o botão de acordo com a vez do jogador
		if(vez == jogador1){

			botao.innerHTML = "<img src='imagens/X.png'>";
			botao.setAttribute("jogada", jogador1);
			vez = jogador2;
		}
		else{

			botao.innerHTML = "<img src='imagens/O.png'>";
			botao.setAttribute("jogada", jogador2);
			vez = jogador1;

		}
		
		ganhou();
		indicarVez();
	}

}

// Zera todos as posições e recomeça o jogo
function resetar(){
	//Seta vez do jogador 1
	vez = jogador1;
	
	//Reinicia a variavel fimDeJogo
	fimDeJogo = false;
	
	//Reinicia o numero de jogadas
	numeroDeJogadas = 0;
	
	//Reseta o html para o original
	document.getElementById("tituloIndicadorDaVez").innerHTML = "É a vez do: <span id='indicadorDaVez'><img src=''></span>"
	document.getElementById("indicadorVencedor").innerHTML = "O vencedor é..."
	
	//Reseta os atributos dos botões
	var casas = document.getElementsByClassName("casa");
	for(var i = 0; i < casas.length; i++){
		casas[i].setAttribute("jogada","");
		casas[i].innerHTML = "";
	}
	indicarVez(); 
}

// Verifica todos as combinações possíveis de se ganhar o jogo ou se deu velha
function ganhou() {
	
	numeroDeJogadas++;

	//Array com as casas(botões)
	var casas = document.getElementsByClassName("casa");
	var vencedor = "";
	var jogadas = [];

	//O array recebe X, O ou "" de acordo com a jogada feita no botão(posição)
	for(var i = 0; i < casas.length; i++){
		jogadas.push(casas[i].getAttribute("jogada"));
	}

	//Verifica todas as possibilidades de ganhar
	if((jogadas[0] == jogadas[1] && jogadas[1] == jogadas[2] && jogadas[2] != "") || (jogadas[0] == jogadas[3] && jogadas[3] == jogadas[6] && jogadas[6] != "") || jogadas[0] == jogadas[4] && jogadas[4] == jogadas[8] && jogadas[8] != ""){
		vencedor = jogadas[0];
	}
	else if((jogadas[3] == jogadas[4] && jogadas[4] == jogadas[5] && jogadas[5] != "") || (jogadas[1] == jogadas[4] && jogadas[4] == jogadas[7] && jogadas[7] != "")){
		vencedor = jogadas[4];
	}
	else if((jogadas[2] == jogadas[4] && jogadas[4] == jogadas[6] && jogadas[6] != "") || (jogadas[2] == jogadas[5] && jogadas[5] == jogadas[8] && jogadas[8] != "")){
		vencedor = jogadas[2];
	}
	else if((jogadas[8] == jogadas[7] && jogadas[7] == jogadas[6] && jogadas[6] != "")){
		vencedor = jogadas[8];
	}
	//Caso onde dá velha
	else if(numeroDeJogadas == 9 && vencedor == ""){
		document.getElementById("indicadorVencedor").innerHTML = "Deu velha!"
		document.getElementById("tituloIndicadorDaVez").innerHTML = "";
		
	}

	//Mostra o vencedor
	if(vencedor != ""){
		
		var imgVencedor = "";
		fimDeJogo = true;

		//Condicional para colocar a imagem corresponde ao vencedor
		if(vencedor == "X"){
			imgVencedor = "<img src='imagens/X.png'>";
		}
		else{
			imgVencedor = "<img src='imagens/O.png'>";
		}

		document.getElementById("indicadorVencedor").innerHTML = "Vencedor é " + imgVencedor;
		document.getElementById("tituloIndicadorDaVez").innerHTML = "";
		vencedor = "";
		
	}
	
}